import React from 'react';

import moment from 'moment';
import { SingleDatePicker } from 'react-dates';
//import 'react-dates/lib/css/_datepicker.css';

export default class ExpenseForm extends React.Component
{
    state = {
        description : this.props.expense ? this.props.expense.description:'',
        amount: this.props.expense?(this.props.expense.amount/100).toString():'',
        note: this.props.expense ? this.props.expense.note:'',
        createdAt : this.props.expense?moment(this.props.expense.createdAt): moment(),
        calendarFocus : false,
        error : ''
    }
    render() {
    return (
        <form  
            className="form"
            onSubmit ={ (e) => {

            
                e.preventDefault();
                if(!this.state.description || !this.state.amount)
                    this.setState(() => ({error:'error'}));
                else{
                    this.setState(() => ({error:''}));
                    
                    this.props.onSubmit({
                        description:this.state.description,
                        amount : parseFloat(this.state.amount,"10")*100,
                        createdAt:this.state.createdAt.valueOf(),
                        note:this.state.note
                    })
                }
            }
        }>   

            {this.state.error &&<p className="form__error">please provide description and amount </p>}
            <input 
                type = 'text'
                placeholder='Description'
                autoFocus 
                className="text-input"
                value = {this.state.description}
                onChange = { (e) => {
                    const description = e.target.value;
                    this.setState(() =>({description}));
                    }
                }
            />

            <input 
                type = 'number' 
                className="text-input"
                placeholder='Amount' 
                value={this.state.amount}
                onChange = {(e) => {
                    let amount = e.target.value;
                    if(amount.match(/^[0-9]{1,}(.[0-9]{0,2})?$/))
                        this.setState(() =>({amount}));
                    }
                } 
            />

            <SingleDatePicker 
                date = {this.state.createdAt}
                onDateChange = {(date) =>  this.setState(() => ({createdAt:date}))}
                focused ={this.state.calendarFocus}
                onFocusChange = {({focused}) => this.setState(() => ({calendarFocus:focused}))}
                isOutsideRange = {() => false }
                numberOfMonths ={1}
            />

            <textarea 
                placeholder='add your Notes (optional)' 
                className="textarea"
                value={this.state.note}
                onChange = {(e) => {
                    const note = e.target.value;
                    this.setState(() => ({note}));
                    }
                }
            />
            <div>
                <button className="button">{this.props.expense?"Save Expense":"Add Expense"}</button>
            </div>
            
        </form>
    
    );
    }
}







