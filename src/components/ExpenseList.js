import React from 'react';
import {connect} from 'react-redux';
import ExpenseListItem  from './ExpenseListItem';
import getExpenses from '../selectors/expenses';


export const ExpenseList = (props) => (
    <div className="list-body">
        <div className="container-content">
        <div className="list-header">
            <div className="show-for-mobile">expenses</div>
            <div className="show-for-desktop">expense</div>
            <div className="show-for-desktop">amount</div>
        </div>
        {
            props.expenses.length === 0 ? (
                <div>
                    <span className="list-item list-item--message">No Expenses</span> 
                </div>
            ):(
                props.expenses.map((expense) => (
                    <ExpenseListItem key = {expense.id} {...expense} />
            )))
        }
        </div>
    </div>
    
);  



const mapStateToProps = (state) => {
    
    const expenses = getExpenses(state.expenses,state.filters);
   
    return {
        expenses 
       
    };
}

export default  connect(mapStateToProps)(ExpenseList);


