import React from 'react';
import {connect} from 'react-redux';
import {setTextFilter,sortByAmount,sortByDate,setStartDate,setEndDate} from '../actions/filters'
import { DateRangePicker }  from 'react-dates';
//import 'react-dates/lib/css/_datepicker.css';


export class ExpenseListFilters extends React.Component
{
    state = {
        calendarFocus:null
    }
    onFocusChange = (calendarFocus) => {
        this.setState(() => ({calendarFocus}));
    }
    onDatesChange = ({startDate,endDate}) => {

        this.props.setStartDate(startDate);
        this.props.setEndDate(endDate);
        
    }
    onSortChange = (e) => {
                
        if(e.target.value === "date") 
            this.props.sortByDate();
        else
            this.props.sortByAmount();     
    }
    onTextChange = (e) => {
                
        this.props.setTextFilter(e.target.value);
        }
    render() {
        return(
            <div className = "container-content">
                <div className="input-group">
                    <div className="input-group__items">
                        <input 
                            type="text"
                            className="text-input"
                            placeholder="Search Expenses" 
                            onChange={this.onTextChange} 
                        />
                    </div>
                    <div className="input-group__items">
                        <select 
                            onChange={this.onSortChange}
                            className="select"
                        >
                            <option value="date" >Date</option>
                            <option value="amount">Amount</option>
                        </select>
                    </div>
                    <div className="input-group__items">
                        <DateRangePicker 
                            startDate = {this.props.filters.startDate}
                            endDate = {this.props.filters.endDate}
                            onDatesChange = {this.onDatesChange}
                            focusedInput={this.state.calendarFocus}
                            onFocusChange={this.onFocusChange}
                            isOutsideRange={()=> false}
                            numberOfMonths={1}
                            showClearDates={true}
                        />
                    </div>
                </div>
            </div>   
        );
    }
}


const mapStateToProps = (state) => ({
    filters : state.filters
});

const mapDispatchToProps =(dispatch) => ({
    setStartDate : (startDate) => dispatch(setStartDate(startDate)),
    setEndDate : (endDate) => dispatch(setEndDate(endDate)),
    setTextFilter :(text) => dispatch(setTextFilter(text)) ,
    sortByDate : () => dispatch(sortByDate()),
    sortByAmount : () => dispatch(sortByAmount())
    

})

export default connect(mapStateToProps,mapDispatchToProps)(ExpenseListFilters);




