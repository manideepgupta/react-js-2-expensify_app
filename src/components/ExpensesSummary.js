import React from 'react';
import {connect} from 'react-redux';
import getExpenses from '../selectors/expenses';
import {getSelectedExpensesTotal} from '../selectors/expensesTotal'
import {Link} from 'react-router-dom';


export const ExpensesSummary = ({length,total}) => {

const amount = new Intl.NumberFormat('en-IN', {
        style: 'currency',
        currency: 'INR'
    }).format(total/100);
    const expense = length==1 ? 'expense' : 'expenses';
    return (
        <div className="page-header">
            <div className="container-content">
                { length>0 &&
                    <h1 className="page-header__title"> 
                        viewing <span>{length}</span> {expense} totalling <span>{amount}</span>
                    </h1>
                }
                <div className="page-header__actions">
                    <Link className="button" to ="/create">Add Expense</Link>
                </div>
            </div>
        </div>   
    );    
};

const mapStateToProps = (state) => {
    const expenses =  getExpenses(state.expenses,state.filters);

    return {
        length:expenses.length,
        total:getSelectedExpensesTotal(expenses)
    } 
};

export default connect(mapStateToProps)(ExpensesSummary);