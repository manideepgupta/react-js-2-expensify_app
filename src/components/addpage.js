import React from 'react';
import ExpenseForm from './ExpenseForm';
import {startAddExpense} from '../actions/expenses';
import {connect} from 'react-redux';

export class AddExpensePage extends React.Component {
    onSubmit = (expense) => {
        this.props.onSubmit(expense);
        this.props.history.push("/");
    }

    render() {
        return (
            <div>
                <div className="page-header"> 
                    <div className="container-content"> 
                        <h1 className="page-header__title">Add Expense</h1>
                    </div>
                </div>
                <div className="container-content">
                    <ExpenseForm 
                        onSubmit = {this.onSubmit}
                    />
                </div>
            </div>    
        );
    }
}
 
const mapDispatchToProps = (dispatch) => ({

        onSubmit:(expense)=>dispatch(startAddExpense(expense))
    })



export default connect(undefined,
    mapDispatchToProps)(AddExpensePage);