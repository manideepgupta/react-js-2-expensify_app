import React from 'react';
import ExpenseForm from './ExpenseForm';
import { connect } from "react-redux";
import {startEditExpense,startRemoveExpense} from "../actions/expenses";



export class EditExpensePage extends React.Component
{
    onSubmit = (expense) => {
        this.props.startEditExpense(this.props.expense.id,expense);
        this.props.history.push("/dashboard");
    }
    onClick = () => {
        this.props.startRemoveExpense(this.props.expense.id);
        this.props.history.push("/dashboard");
    }

    render() {
        return (
            <div>
                <div className="page-header">
                    <div className="container-content">
                        <h1 className="page-header__title">Edit Expense</h1>
                    </div>
                </div>
                <div className="container-content">
                    <ExpenseForm 
                        expense={this.props.expense}
                        onSubmit = {this.onSubmit}
                    />
                    
                        <button 
                            className="button button__secondary"
                            onClick = {this.onClick}
                        >
                        Remove Expense
                        </button>
                    
                
                </div>
            </div>  
        );
    }
}

const mapPropsToState    = (state,props) => {

    return {
        expense:state.expenses.find((expense) => expense.id===props.match.params.id )
        
    }
}
const mapDispatchToState = (dispatch) => {
    return {
        startEditExpense : (id,expense) => dispatch(startEditExpense(id,expense)),
        startRemoveExpense : (id) => dispatch(startRemoveExpense(id))
    }
}
export default connect(mapPropsToState,mapDispatchToState)(EditExpensePage);