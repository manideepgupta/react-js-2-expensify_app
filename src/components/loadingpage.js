import React from 'react';

const LoadingPage = () => (
    <div class="loader">
        <img className="loader__image" src="/images/loading.webp"/>
    </div>
)

export default LoadingPage;