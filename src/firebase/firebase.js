import * as firebase from 'firebase';

const firebaseConfig = {
    apiKey: process.env.FIREBASE_API_KEY,
    authDomain: process.env.FIREBASE_AUTH_DOMAIN,
    databaseURL: process.env.FIRREBASE_DATABASE_URL,
    projectId: process.env.FIREBASE_PROJECT_ID,
    storageBucket: process.env.FIREBASE_STORAGE_BUCKET,
    messagingSenderId: process.env.FIREBASE_MESSAGING_SENDER_ID

    
  };
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

const googleAuthProvider = new firebase.auth.GoogleAuthProvider();
const database  = firebase.database();

export {firebase , googleAuthProvider, database as default };

// const expenses = [{
//     description : "Rent",
//     amount:3500,
//     createdAt : 123,
//     note : "paid to the owner"
// },{
//     description : "Bill",
//     amount:500,
//     createdAt :127,
//     note : "paid to the APDSCL"
// },{
//     description : "Grocery",
//     amount:1000,
//     createdAt : 165,
//     note : "paid to the store"
// }]

// database.ref('expenses').push(expenses[0]);
// database.ref('expenses').push(expenses[1]);
// database.ref('expenses').push(expenses[2]);


// database.ref('expenses')
//     .on('value' , (snapshot) => {
//         const expenses = [];
//         snapshot.forEach(element => {
//             expenses.push({
//                 id:element.key,
//                 ...element.val()
//             })
//         });
//         console.log(expenses);
//     })















// const onValueChange = (snapshot) => {
//     const fetchedData = snapshot.val()
//     console.log(`${fetchedData.name} is a ${fetchedData.job.role} at ${fetchedData.job.company}`);
// }
// database.ref().on('value',onValueChange);
// database.ref().set({
//     name:'Manideep',
//     age:21,
//     stresslevel:6,
//     job:{
//         role:'software developer',
//         company : 'Google'
//     },
//     location:{
//         city:'Uppugundur',
//         country:'india'
//     }

// }).then(() => {
//     console.log('data is saved!');
// }).catch((e) => {
//     console.log('this is the error',e);
// })

// database.ref('isSingle')
//     .remove() //.set(null)
//     .then(() => {
//         console.log('Data is removed !');
//     }).catch((error) => {
//         console.log('data didnot removed',error.message)
//     })
// database.ref().update({
//     stresslevel:9,
//     'location/city':'Ongole',
//     'job/company':'Microsoft'
// }).then(() => {
//     console.log('data is updated');
// }).catch((e) => {
//     console.log('data didn\'t updated!',e.message);
// })

  
  