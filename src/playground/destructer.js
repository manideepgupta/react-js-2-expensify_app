//
//objects 
//

const book = {
    name : 'manideep',
    author : 'asf',
    publisher : {
        //name : 'penguin'
    }

};

const {name:publisherName = 'anonymous'} = book.publisher;

console.log(publisherName);


//
//arrays
//

const item = ['Coffee (hot)', '$2', '$2.50', '$2.75'];
const [itemName, ,mediumPrice,] = item;
console.log(`A medium ${itemName} costs ${mediumPrice}.`);