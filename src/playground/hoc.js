import React from 'react';
import ReactDOM from 'react-dom';

const Info = (props) => (
    <div>
        <h1>INFO</h1>
        <p>This is info {props.info}</p>
    </div>
);

const requireAuthentication= (wrappedComponent) => {
    return (props) => (
        <div>
            {props.isAuthentiacted ? <Info {...props}/> : "please login to get Info. "}
        </div>
    );
}


const AuthInfo = requireAuthentication(Info);
ReactDOM.render(<AuthInfo isAuthentiacted={true} info ="There are the details."/>,document.getElementById('app'));