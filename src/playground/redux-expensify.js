import {createStore,combineReducers } from 'redux';
import uuid from 'uuid';
//ADD_EXPENSE
const addExpense = ({ 
        description='', 
        note='', 
        amount=0, 
        createdAt=0 
    }
) => ({
        type : 'ADD_EXPENSE',
        expense :
        {
            id : uuid(),
            description,
            note,
            amount,
            createdAt
        }
    });

//REMOVE_EXPENSE

const removeExpense = ({id}) => ({
        type:'REMOVE_EXPENSE',
        id
    }
); 

//EDIT_EXPENSE

const editExpense = (id,updates) => ({

        type:'EDIT_EXPENSE',
        id,
        updates
    }
);

//SET-TEXT_FILTER
const setTextFilter= (text = '') => ({
    type:'SET_TEXT_FILTER',
    text
});

//SORT-BY-DATE
const sortByDate = () => ({
    type: 'SORT_BY_DATE'
});

//SORT_BY_AMOUNT
const sortByAmount = () => ({
    type: 'SORT_BY_AMOUNT'
});

//SET_START_DATE
const setStartDate = (date) => ({
    type:'SET_START_DATE',
    date
})

//SET_END_DATE
const setEndDate = (date) => ({
    type:'SET_END_DATE',
    date
})

//get expenses
const getExpenses = (expenses,filters) => {
    return expenses.filter((expense) => {
        const startDateMatch = typeof filters.startDate !== 'number' || expense.createdAt >=startDate;
        const endDateMaatch = typeof filters.startDate !== 'number' || expense.createdAt <=endDate;
        const textMatch = expense.description.toLowerCase().includes(filters.text.toLowerCase());
        return startDateMatch&&endDateMaatch&&textMatch;
    }).sort((a,b) => {
        if(filters.sortBy === 'Date') 
            return b.createdAt - a.createdAt;
        if(filters.sortBy === 'Amount')
            return b.amount -a.amount;
    });
    
}


const ExpensesReducerDefaultValue = [];

const ExpensesReducer = (state = ExpensesReducerDefaultValue,action) => {
    
    switch(action.type){

        case 'ADD_EXPENSE' : 
            return [
                ...state,
                action.expense
            ]

        case 'REMOVE_EXPENSE' :
            return state.filter(({id}) => action.id !== id );

        case 'EDIT_EXPENSE' :
            return state.map((expense) => (
                (expense.id === action.id) ? {...expense,...action.updates } :expense
            )); 

        default :  
            return state;
    }
}

const FiltersReducerDefaultValue = {
    text:'',
    sortBy:'Date' ,//date or amount
    startDate : undefined,
    endDate : undefined
}

const FilterReducer = (state=FiltersReducerDefaultValue,action) => {
    switch(action.type){
        case 'SET_TEXT_FILTER' :
            return {...state,text:action.text};
        case 'SORT_BY_DATE' :
            return {...state,sortBy:"Date"};

        case 'SORT_BY_AMOUNT' :
            return {...state,sortBy:"Amount"};
        case 'SET_START_DATE' :
            return {...state,startDate:action.date}  
        case 'SET_END_DATE' :
            return {...state,endDate:action.date}
        default:
            return state;
    }
}

const store = createStore(
    combineReducers({
        expenses : ExpensesReducer,
        filters : FilterReducer
    })
);

store.subscribe(() => {
    const state = store.getState();
    const expenses = getExpenses(state.expenses,state.filters);
    console.log(expenses);
});

const ExpenseOne = store.dispatch(addExpense({description:'Rent',amount:10000,createdAt:100}));
const ExpenseTwo = store.dispatch(addExpense({description:'Bill',amount:300,createdAt:300}));

// store.dispatch(removeExpense({id:ExpenseTwo.expense.id}));

// store.dispatch(editExpense(ExpenseOne.expense.id,{amount:400}));

// store.dispatch(setTextFilter('Rentingjvksdbvd'));
store.dispatch(setTextFilter(''));

// store.dispatch(sortByDate());
store.dispatch(sortByAmount());

//store.dispatch(setStartDate(125));
//store.dispatch(setStartDate());
//store.dispatch(setEndDate(1250));
