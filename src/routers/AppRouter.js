import React from 'react';
import { createBrowserHistory } from 'history';
import {Router,Route,Switch,Link,NavLink} from 'react-router-dom';

import ExpensifyDashboardPage from '../components/dashboard'
import EditExpensePage from '../components/editpage'
import AddExpensePage from '../components/addpage'
import HelpPage from '../components/helppage'
import NotFoundPage from '../components/notfoundpage'
import Login from '../components/loginpage';
import PrivateRoute from './PrivateRoute';
import PublicRoute from './PublicRoute';



export const history = createBrowserHistory();
const AppRouter = () => (
    
        <Router history={history}>
            <div>
                
                <Switch>
                    <PublicRoute path = "/" component = {Login} exact={true} />
                    <PrivateRoute path = "/dashboard" component={ExpensifyDashboardPage} />
                    <PrivateRoute path = "/create" component={AddExpensePage}  />
                    <PrivateRoute path = "/edit/:id" component={EditExpensePage}  />
                    <Route path = "/help" component={HelpPage}  />
                    <Route component={NotFoundPage}  />
                </Switch>
            </div>
        </Router>
   
);


export default AppRouter;