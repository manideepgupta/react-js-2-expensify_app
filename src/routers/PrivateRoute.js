import React from 'react';
import {connect} from 'react-redux';
import {Route,Redirect} from 'react-router-dom';
import Header from '../components/header.js';

const PrivateRoute = ({
    isAuthenticated,
    component:Component,
    ...rest
}) => (
    <Route {...rest} component= {(props) => (
            isAuthenticated ? (
                <div>
                    <Header />
                    <Component {...props} />
                </div> 
            ): (
                < Redirect to = '/' />
            )
        )}
    />
);

const mapPropsToState = (state) => ({
    isAuthenticated : !!state.auth.uid
})

export default connect(mapPropsToState)(PrivateRoute);