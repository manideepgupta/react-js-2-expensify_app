import moment from 'moment';

//get expenses
const getExpenses = (expenses,filters) => {
    
    return expenses.filter((expense) => {
        const createdAt = moment(expense.createdAt);
        const startDateMatch = filters.startDate?filters.startDate.isSameOrBefore(createdAt):true;
        const endDateMaatch = filters.endDate?filters.endDate.isSameOrAfter(createdAt):true;
        const textMatch = expense.description.toLowerCase().includes(filters.text.toLowerCase());
        return startDateMatch&&endDateMaatch&&textMatch;
    }).sort((a,b) => {
        if(filters.sortBy === 'Date') 
            return b.createdAt - a.createdAt;
        if(filters.sortBy === 'Amount')
            return b.amount -a.amount;
    });
    
}

export default getExpenses;