import {createStore,combineReducers,applyMiddleware ,compose} from 'redux';
import ExpensesReducer from '../reducers/expenses';
import FilterReducer from '../reducers/filters';
import AuthReducer from '../reducers/auth';
import thunk from 'redux-thunk';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default () => {
    const store = createStore(
        combineReducers({
            expenses : ExpensesReducer,
            filters : FilterReducer,
            auth : AuthReducer
        }),
        composeEnhancers(applyMiddleware(thunk))
        //window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
    );
    return store;
}
