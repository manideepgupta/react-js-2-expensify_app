import { startAddExpense,addExpense,editExpense,removeExpense } from "../../actions/expenses";
import thunk from 'redux-thunk';
import configureStore from 'redux-mock-store';
import database from '../../firebase/firebase'

const createStore = configureStore([thunk]);


test('should remove expense from  expenses',()=>{
    expect(removeExpense('1234')).toEqual({
        type:'REMOVE_EXPENSE',
        id:'1234'

    })

})

test('should edit expense in expense objects',()=> {
    expect(editExpense('1234',{description : 'change'})).toEqual({
        type:'EDIT_EXPENSE',
        id:'1234',
        updates:{
            description:'change'
        }
    }) 
    
    

})


// test('shoud ass expense in store and datsabse ',(done) =>  {
//     const store = createStore({});
//     store.dispatch(startAddExpense()).then(() => {
//         const actions = store.getActions();
//         expect(actions[0].expense).toEqual({
//            description:'', 
//            note:'', 
//            amount:0, 
//            createdAt:0,
//            id:expect.any(String)
//         });
//        return database.ref(`expenses/${actions[0].expense.id}`).once('value');
//     }).then((snapshot) => {
//         expect(snapshot.val()).toEqual({
//            description:'', 
//            note:'', 
//            amount:0, 
//            createdAt:0,
//         });
//         done(); 
        
        
//     })
   
// })