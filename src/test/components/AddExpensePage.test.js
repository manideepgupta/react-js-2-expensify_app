import {AddExpensePage} from '../../components/addpage';
import {shallow} from 'enzyme';
import React from 'react';




test('should test add expense page',() => {
    const expense = {
        description : 'RENT34',
        amount:300,
        note:'zhjgsdhj',
        createdAt:123
    }
    const onSubmit = jest.fn();
    const history = {push:jest.fn()};
    const wrapper = shallow(<AddExpensePage onSubmit={onSubmit} history={history}/>);
    expect(wrapper).toMatchSnapshot();
})

test('should test add expense page with arguments',() => {
    const expense = {
        description : 'RENT34',
        amount:300,
        note:'zhjgsdhj',
        createdAt:123
    }
    const onSubmit = jest.fn();
    const history = {push:jest.fn()};
    const wrapper = shallow(<AddExpensePage onSubmit={onSubmit} history={history}/>);
    wrapper.find('ExpenseForm').prop('onSubmit')(expense);
    expect(onSubmit).toHaveBeenLastCalledWith({
        amount:300,
        note:'zhjgsdhj',
        createdAt:123,
        description:'RENT34'
    });
    expect(history.push).toHaveBeenLastCalledWith('/');
    
})



