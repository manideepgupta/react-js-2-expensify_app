import {EditExpensePage} from '../../components/editpage';
import {shallow} from 'enzyme';
import React from 'react';



test('should test EditExpensePage',() => {
    const match = {
        params : {
            id:'123'
        }
    }
    const expense = {
        description : 'RENT34',
        amount:300,
        note:'zhjgsdhj',
        createdAt:123,
        id:'123'
    
    }
    const removeExpense = jest.fn();
    const editExpense = jest.fn();
    const history = {
        push : jest.fn()
    }
    const wrapper = shallow(<EditExpensePage match={match} history={history} expense={expense} editExpense={editExpense} removeExpense={removeExpense}/>);
    expect(wrapper).toMatchSnapshot();

})

test('should test EditExpense',() => {
    const match = {
        params : {
            id:'123'
        }
    }
    const expense = {
        description : 'RENT34',
        amount:300,
        note:'zhjgsdhj',
        createdAt:123,
        id:'123'
    
    }
    const removeExpense = jest.fn();
    const editExpense = jest.fn();
    const history = {
        push : jest.fn()
    }
    const wrapper = shallow(<EditExpensePage match={match} history={history} expense={expense} editExpense={editExpense} removeExpense={removeExpense}/>);
    wrapper.find('ExpenseForm').prop('onSubmit')(expense);
    expect(editExpense).toHaveBeenLastCalledWith(expense.id,expense);
    expect(history.push).toHaveBeenLastCalledWith('/');
    
})


test('should test EditExpense',() => {
    const match = {
        params : {
            id:'123'
        }
    }
    const expense = {
        description : 'RENT34',
        amount:300,
        note:'zhjgsdhj',
        createdAt:123,
        id:'123'
    
    }
    const removeExpense = jest.fn();
    const editExpense = jest.fn();
    const history = {
        push : jest.fn()
    }
    const wrapper = shallow(<EditExpensePage match={match} history={history} expense={expense} editExpense={editExpense} removeExpense={removeExpense}/>);
    wrapper.find('button').simulate('click');
    expect(removeExpense).toHaveBeenLastCalledWith('123');
    expect(history.push).toHaveBeenLastCalledWith('/');
    
})