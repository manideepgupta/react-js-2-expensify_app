import {shallow} from 'enzyme';
import React from 'react';
import {ExpensesSummary} from '../../components/ExpensesSummary';


test('should render expense suumary page',() => {
    const wrapper = shallow(<ExpensesSummary length={2} total={100} />);
    expect(wrapper).toMatchSnapshot();
})
