import React from 'react';
import {shallow} from 'enzyme';
import ExpenseForm from '../../components/ExpenseForm';



test ('testing the expense form ',() => {
    const wrapper = shallow(<ExpenseForm />);
    expect(wrapper).toMatchSnapshot();
})

test('testing the edit expense',() => {
    
    const expense = {
        description : 'RENT34',
        amount:300,
        note:'zhjgsdhj'
    }
    const wrapper = shallow(<ExpenseForm expense={expense}/>)
    expect(wrapper).toMatchSnapshot();
    
})

test('testing the textarea element',() => {
    const value = 'ajkfbdsfkj';
    const wrapper = shallow(<ExpenseForm />);
    expect(wrapper.state('note')).toBe('');
    wrapper.find('textarea').simulate('change',{target:{value}});
    expect(wrapper.state('note')).toBe(value);
})


test('testing the input amount element',() => {
    const value = '123.245';
    const wrapper = shallow(<ExpenseForm />);
    expect(wrapper.state('amount')).toBe('');
    wrapper.find('input').at(1).simulate('change',{target:{value}});
    expect(wrapper.state('amount')).toBe('');
})

test('testing the  submission form validation',() => {
    const onSubmitSpy = jest.fn();
    const expense = {
        description : 'RENT34',
        amount:300,
        note:'zhjgsdhj',
        createdAt:123
    }
    const wrapper = shallow(<ExpenseForm expense={expense} onSubmit={onSubmitSpy}/>);
    wrapper.find('form').simulate('submit',{
        preventDefault : () => {}
    });
    expect(wrapper.state('error')).toBe('');
    expect(onSubmitSpy).toHaveBeenCalledWith(expense);

})


test('testing the singledatepicker onfocus change',() => {
    const expense = {
        description : 'RENT34',
        amount:300,
        note:'zhjgsdhj',
        createdAt:123
    }
    const wrapper = shallow(<ExpenseForm expense={expense} />);
    expect(wrapper.state('calendarFocus')).toBe(false);
    wrapper.find('SingleDatePicker').prop('onFocusChange')({focused:true});
    expect(wrapper.state('calendarFocus')).toBe(true);
})
 