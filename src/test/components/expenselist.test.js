import {ExpenseList}  from '../../components/ExpenseList';

import {shallow} from 'enzyme';
import React from 'react';
const expenses = [
    {
        id:"1",
        description:"rent",
        note:'',
        amount:100,
        createdAt:-1000
    },{
       id:"2",
        description:"rent2",
        note:'',
        amount:1000,
        createdAt:-2000
    },{
       id:"13",
       description:"rent3",
       note:'',
       amount:300,
       createdAt:-1500
    }
     
]; 


test('should render expenselist', () =>  {
    const wrapper = shallow(<ExpenseList expenses={expenses}/>);
    expect(wrapper).toMatchSnapshot();
})


test('should render expenselist with empty list', () =>  {
    const wrapper = shallow(<ExpenseList expenses={[]}/>);
    expect(wrapper).toMatchSnapshot();
})

