import React from 'react';
import {shallow} from 'enzyme';
import {ExpenseListFilters} from '../../components/ExpenseListFilters';
import moment from 'moment';

test('testing the expense list filters',() => {
    const filters = {
        text:'mani',
        sortBy:'Date',
        startDate:moment(),
        endDate:moment()
    }
    const setTextFilter = jest.fn();
    const setStartDate = jest.fn();
    const sortByDate = jest.fn();
    const sortByAmount = jest.fn();
    const setEndDate = jest.fn();

    const wrapper = shallow(<ExpenseListFilters 
        filters={filters}
        setTextFilter={setTextFilter}
        setStartDate={setStartDate}
        setEndDate={setEndDate}
        sortByAmount={sortByAmount}
        sortByDate={sortByDate}
         />
        );

    expect(wrapper).toMatchSnapshot();
})


test('test the set text filter',() => {
    
    const filters = {
        text:'mani',
        sortBy:'Date',
        startDate:undefined,
        endDate:undefined
    }
    const setTextFilter = jest.fn();
    const setStartDate = jest.fn();
    const sortByDate = jest.fn();
    const sortByAmount = jest.fn();
    const setEndDate = jest.fn();

    const wrapper = shallow(<ExpenseListFilters 
        filters={filters}
        setTextFilter={setTextFilter}
        setStartDate={setStartDate}
        setEndDate={setEndDate}
        sortByAmount={sortByAmount}
        sortByDate={sortByDate}
         />
        );
    const value = 'mani'
     wrapper.find('input').simulate('change',{target :{value}})
     expect(setTextFilter).toHaveBeenLastCalledWith('mani');   
    
})

test('test the startDate and end Date',() => {
    const filters = {
        text:'mani',
        sortBy:'Date',
        startDate:undefined,
        endDate:undefined
    }
    const setTextFilter = jest.fn();
    const setStartDate = jest.fn();
    const sortByDate = jest.fn();
    const sortByAmount = jest.fn();
    const setEndDate = jest.fn();

    const wrapper = shallow(<ExpenseListFilters 
        filters={filters}
        setTextFilter={setTextFilter}
        setStartDate={setStartDate}
        setEndDate={setEndDate}
        sortByAmount={sortByAmount}
        sortByDate={sortByDate}
         />
        );
    
    const startDate = moment();
    const endDate = moment();
    
    wrapper.find('DateRangePicker').prop('onDatesChange')({startDate,endDate});
    expect(setStartDate).toHaveBeenLastCalledWith(moment());
    expect(setEndDate).toHaveBeenLastCalledWith(moment());
    
})

test('test the sortby date',() => {
    const filters = {
        text:'mani',
        sortBy:'Date',
        startDate:undefined,
        endDate:undefined
    }
    const setTextFilter = jest.fn();
    const setStartDate = jest.fn();
    const sortByDate = jest.fn();
    const sortByAmount = jest.fn();
    const setEndDate = jest.fn();

    const wrapper = shallow(<ExpenseListFilters 
        filters={filters}
        setTextFilter={setTextFilter}
        setStartDate={setStartDate}
        setEndDate={setEndDate}
        sortByAmount={sortByAmount}
        sortByDate={sortByDate}
         />
        );
    
    const value = 'date'
    wrapper.find('select').simulate('change',{target :{value}});
    expect(sortByDate).toHaveBeenLastCalledWith();
    
})

test('test the sortby amount',() => {
    const filters = {
        text:'mani',
        sortBy:'Date',
        startDate:undefined,
        endDate:undefined
    }
    const setTextFilter = jest.fn();
    const setStartDate = jest.fn();
    const sortByDate = jest.fn();
    const sortByAmount = jest.fn();
    const setEndDate = jest.fn();
    


    const wrapper = shallow(<ExpenseListFilters 
        filters={filters}
        setTextFilter={setTextFilter}
        setStartDate={setStartDate}
        setEndDate={setEndDate}
        sortByAmount={sortByAmount}
        sortByDate={sortByDate}
        
         />
        );
    
    const value = 'amount';
    wrapper.find('select').simulate('change',{target :{value}});
    expect(sortByAmount).toHaveBeenLastCalledWith();
    
})


test('test the focuschange',() => {
    const filters = {
        text:'mani',
        sortBy:'Date',
        startDate:undefined,
        endDate:undefined
    }
    const setTextFilter = jest.fn();
    const setStartDate = jest.fn();
    const sortByDate = jest.fn();
    const sortByAmount = jest.fn();
    const setEndDate = jest.fn();
    


    const wrapper = shallow(<ExpenseListFilters 
        filters={filters}
        setTextFilter={setTextFilter}
        setStartDate={setStartDate}
        setEndDate={setEndDate}
        sortByAmount={sortByAmount}
        sortByDate={sortByDate}
        
         />
        );
    
    const value = 'amount';
    wrapper.find('DateRangePicker').prop('onFocusChange')(true)
    expect(wrapper.state('calendarFocus')).toBe(null)
})

