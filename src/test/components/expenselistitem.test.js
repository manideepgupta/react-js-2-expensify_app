import ExpenseListItem from '../../components/ExpenseListItem'
import {shallow} from 'enzyme';
import React from 'react';

const expense = {
    description:'Rent',
    amount:100,
    createdAt:120,
    id:'123'
}

test('testing the expenselist item ',() =>{
    const wrapper = shallow(<ExpenseListItem {...expense}/>);
    expect(wrapper).toMatchSnapshot();
})

