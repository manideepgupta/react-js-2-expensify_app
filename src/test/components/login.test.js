import {shallow} from 'enzyme';
import React from 'react';
import {Login} from '../../components/loginpage';


test('should render login page correctly',() => {
    const wrapper = shallow(<Login />);
    expect(wrapper).toMatchSnapshot();
});