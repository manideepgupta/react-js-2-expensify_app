import ExpensesReducer from '../../reducers/expenses';
const expenses = [
    {
        id:"1",
        description:"rent",
        note:'',
        amount:100,
        createdAt:-1000
    },{
       id:"2",
        description:"rent2",
        note:'',
        amount:1000,
        createdAt:-2000
    },{
       id:"13",
       description:"rent3",
       note:'',
       amount:300,
       createdAt:-1500
    }
     
]; 

test('should test expense reducer',() => {
    const state = ExpensesReducer(undefined,{type:'@@INIT'});
    expect(state).toEqual([]);   
})


test('should test add expense to reducer',()=> {
    const expense = {
        id:'123',
        description:'Rent4'
    }
    const state = ExpensesReducer(expenses,{
        type:'ADD_EXPENSE',
        expense
    })
    expect(state).toEqual([...expenses,expense]);
})


test('should test remove expense to reducer',()=> {
    
    const state = ExpensesReducer(expenses,{
        type:'REMOVE_EXPENSE',
        id:'13'
    })
    expect(state).toEqual([expenses[0],expenses[1]]);
})



test('should test remove expense to reducer id not found',()=> {
    
    const state = ExpensesReducer(expenses,{
        type:'REMOVE_EXPENSE',
        id:'134'
    })
    expect(state).toEqual(expenses);
})


test('should test edit expense to reducer id not found',()=> {
    
    const state = ExpensesReducer(expenses,{
        type:'EDIT_EXPENSE',
        id:'134',
        updates:{
            description:"RENT567"
        }
    })
    expect(state).toEqual(expenses);
})

test('should test edit expense to reducer ',()=> {
    const updates= {
        description:"RENT567"
    }
    const state = ExpensesReducer(expenses,{
        type:'EDIT_EXPENSE',
        id:'13',
        updates
    })
    expect(state).toEqual([expenses[0],expenses[1],{...expenses[3],...updates}]);
})
