import FilterReducer from '../../reducers/filters'
const filters = {
    text:'',
    sortBy:'Date',
    startDate:undefined,
    endDate:undefined

}

test('should set default filtrt values',() => {
  const  state = FilterReducer(undefined,{ type:'@@INIT'}) ;
  expect(state).toEqual(filters); 
})