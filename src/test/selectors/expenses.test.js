import  getExpenses  from "../../selectors/expenses";
import moment from 'moment';

 const expenses = [
     {
         id:"1",
         description:"rent",
         note:'',
         amount:100,
         createdAt:-1000
     },{
        id:"2",
         description:"rent2",
         note:'',
         amount:1000,
         createdAt:-2000
     },{
        id:"13",
        description:"rent3",
        note:'',
        amount:300,
        createdAt:-1500
     }
      
];

const filters = {
    sortBy:'Amount',
    text:'rent',
    startDate:moment(-3000),
    endDate:moment(0)
}

test('should setup getexpenses selector object',() => {
    const visitedExpenses = getExpenses(expenses,filters);
    expect(visitedExpenses).toEqual([
        {
            id:"1",
            description:"rent",
            note:'',
            amount:100,
            createdAt:-1000
        },{
            id:"13",
            description:"rent3",
            note:'',
            amount:300,
            createdAt:-1500
        },{
            id:"2",
             description:"rent2",
             note:'',
             amount:1000,
             createdAt:-2000
        }
    ]) 
})