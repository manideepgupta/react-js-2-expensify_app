
import {getSelectedExpensesTotal} from '../../selectors/expensesTotal';

const expenses = [
    {
        id:"1",
        description:"rent",
        note:'',
        amount:100,
        createdAt:-1000
    },{
       id:"2",
        description:"rent2",
        note:'',
        amount:1000,
        createdAt:-2000
    },{
       id:"13",
       description:"rent3",
       note:'',
       amount:300,
       createdAt:-1500
    }
     
]; 




test('should test total 0 if no expense',() => {
    const total = getSelectedExpensesTotal([{
        id:"13",
        description:"rent3",
        note:'',
        amount:300,
        createdAt:-1500
     }]);
    expect(total).toBe(300);
})

